#include <fcntl.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	mode_t const mode = 0644;
	int status;

	for (int i = 1; i < argc; ++i)
	{
		status = creat(argv[i], mode);

		if (status < 0)
		{
			perror("TEST");
			return 1;
		}
	}

	return 0;
}
