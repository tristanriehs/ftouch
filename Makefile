PREFIX = /usr/local

CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0 -ggdb

BIN = ftouch
SRC = ftouch.c
OBJ = ftouch.o

.PHONY: all compile clean install uninstall

all: compile

compile: $(BIN)

$(BIN): $(OBJ)
	cc $^ -o $@

%.o: %.c
	cc $(CFLAGS) -c $< -o $@

clean:
	rm -f $(BIN) $(OBJ)

install: compile
	cp -f $(BIN) $(PREFIX)/bin

uninstall: clean
	rm -f $(PREFIX)/bin/$(BIN)
